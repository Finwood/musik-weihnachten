\include "summertime.ly"

\version "2.18.2"
\include "../paper.ly"

\header {
  instrument = "Violine"
}

\score {
  \new Staff \with {
    % instrumentName = #"Violine "
    % shortInstrumentName = #"Vl. "
    midiInstrument = #"violin"
  } \vl

  \midi {
    \tempo 4. = 56
  }

  \layout {}
}
