\include "summertime.ly"

\version "2.18.2"
\include "../paper.ly"
#(set-global-staff-size 16)

\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = #"Violine "
      shortInstrumentName = #"Vl. "
      midiInstrument = #"violin"
    } \vl

    \new Staff \with {
      instrumentName = #"Sopransaxophon "
      shortInstrumentName = #"Sax. "
      midiInstrument = #"soprano sax"
    } \sax

    \new Staff \with {
      instrumentName = #"Cello "
      shortInstrumentName = #"Vc. "
      midiInstrument = #"viola"
    } \vlc
  >>

  \midi {
    \tempo 4. = 56
  }

  \layout {
    \context {
      \Staff \RemoveEmptyStaves
    }
  }
}
