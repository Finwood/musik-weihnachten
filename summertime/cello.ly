\include "summertime.ly"

\version "2.18.2"
\include "../paper.ly"

\header {
  instrument = "Cello"
}

\score {
  \new Staff \with {
    % instrumentName = #"Cello "
    % shortInstrumentName = #"Vc. "
    midiInstrument = #"viola"
  } \vlc

  \midi {
    \tempo 4. = 56
  }

  \layout {}
}
