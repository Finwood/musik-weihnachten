

\version "2.18.2"

\pointAndClickOff




Sax = \new Staff \with {
instrumentName = #"Sopran Sax. "
midiInstrument = #"soprano sax"
}

{
%\transpose bes c' {
\relative c''{
%\transposition bes,

	\clef "treble"
	\key f \major
	\time 12/8
	\tempo 4. = 44


% Intro --------------------------------------------------

	r1. | r | r |

% 1st verse ----------------------------------------------

	r4\mf^"1st verse" f,16 f~ f2. r4. | r2. d8 e f g a8. g16 | a4. r8 g16 e8. f4. r16 d f e f f |
	c'4.\<~( c4 c16 d c4.\!) r8 es\mf( des | c) des16( c c bes) bes4. r8 c16 g bes a c8 bes r16 d16( | c bes c bes8.~ bes4. c8) r c( bes) a( f |
	g16 f a4) a g16 f~ f4. r4 d16( e~ | e f bes4~ bes8) r a bes16( a bes c~ c8) r c( bes | as8 g16 es f8) r4 g8 as16( g f g d8) d16 es es8 es16( f~ |
	\time 6/8
	f2.) |
	\time 12/8
	r1. | r | r2. r4. r16 d'( es8 c16 bes |

% 2nd verse ----------------------------------------------

	c2.^"2nd verse") r | d,8 e f g a8. a16 bes4. r8 d, e16 f~ | f4. r2. r16 d f e f f |
	c'4.\<~( c4 c16 d c4.\!) r8 es\mf( des | c) des16( c c bes) bes4. r8 c16 g bes a c8 bes r16 d16( | c bes c bes8.~ bes4. c8) r c( bes) a( f |
	g16 f a4) a g16 f~ f4. r4 d16( e~ | e f bes4~ bes8) r a bes16( a bes c~ c8) r c( bes | as8 g16 es f8) r4 g8 as16( g f g d8) d16 es es8 es16( f~ |
	\time 6/8
	f2.) |
	\time 12/8
	r1. | r | r2. r4. r16 d'\p( es8 c16 bes |

% Outro --------------------------------------------------

	c4.^"Outro - rit.") r8 f,16 c'8 c16 c( es c bes c8) r4 c8 | c bes r a a bes r16 f g8 as16( g f g d es8. | f4.) r4. r2.\fermata |



\bar "|."
}}%}






Geige = \new Staff \with {
instrumentName = #"Violine "
midiInstrument = #"violin"
}

{
\relative c''{

	\clef "treble"
	\key f \major
	\time 12/8
	\tempo 4. = 44

	\set Staff.midiMaximumVolume = #0.7


% Intro --------------------------------------------------

	f8\mf^"Intro" e16 f g a g8 f e16 f d8 e f d c d | bes c d a bes c g a bes c c c | d c16 bes c a bes a g f d c d f g a bes c d8\> c bes |

% 1st verse ----------------------------------------------

	a4.\mp a16 g f8 g a c e e d c16 d | f4 f8 f16 e d e c d bes c a bes g a f g a8 g | a4. a16 c bes a g f g a a8 a a16\< bes a g f g |
	a a a a a a a bes a g f g a a a a a a a bes a g f g\! | bes4\mp bes16 c d c bes c a bes a c bes a d c bes8 d c | bes2. bes16 c a bes g a bes8 a bes |
	a4. a16 g f g a bes c8 d4 d8 c bes | c a a a4 a8 a\glissando( g) g g\glissando( f4) | a8 g g g g g a8 g g g f g |
	\time 6/8
	a4.\< a16 bes c bes a bes
	\time 12/8
	d16\mf c bes c a bes c d c bes c a bes8 c d f e d | bes4. a16 bes c d c bes g8 g a bes c c | c4\> c8 bes4 bes8 a4 a8 bes4.\! |

% 2nd verse ----------------------------------------------

	r8 f16\mf g a bes c d c4 a8 c a e a c | bes4 bes16 c d8 f f f bes bes bes a g | f4 f,16\pp e f8 e c d4 f16 e f8 d e |
	f4. f f f8 a4 | bes4. bes bes bes8 a4 | g4. g g g8 f4 |
	<f a>4. <f a> <f a> <f a> | <f a> <f a> <f a> <f a> | bes1. |
	\time 6/8
	<a c>2. |
	\time 12/8
	<c f>4.\f <c e> <d>~ <d>8 <c e> <c f> | <bes g'>4.\> <a f'> <g e'> <f d'> | <e c'> <d bes'>~ <d bes'>2.\! |

% Outro --------------------------------------------------

	d'16\p( c) c8 c c c c d16( c) c8 c f e f | d16( c) c8 c d16( c) c8 c bes bes bes c16( bes) bes8 bes | a16 c f e c a a g f g a a f2.\fermata |


\bar "|."
}}






Cello = \new Staff \with {
instrumentName = #"Cello "
midiInstrument = #"viola"
}

{
\relative c{

	\clef "bass"
	\key f \major
	\time 12/8
	\tempo 4. = 44


% Intro --------------------------------------------------

	f4.\mp e d d8 e f | g4. f e d | c bes~ bes2. |

% 1st verse ----------------------------------------------

	f'4. f a, a | bes bes bes bes4 c8 | f4. f f f8 d c |
	f,4. f8 a c f4. e8 d c | bes4. bes bes bes8 a4 | g4. g8 bes d g4. g |
	f f e e | d d c c | bes1. |
	\time 6/8
	f2. |
	\time 12/8
	f'4.\f e d d8 e f | g4.\> f e d | c bes~ bes2.\! |

% 2nd verse ----------------------------------------------

	f'4.\mp f a, a | bes bes bes bes4 c8 | f4. a16\mf c bes a g f g a a8 a a16 bes a g f g |
	a a a a a a a16 bes a g f g a a a a a a a16 bes a g f g | bes4 bes16 c d c bes c a bes a c bes a d c bes8 d c | bes4. bes8 d c bes16 c a bes g a bes8 a bes |
	a4. a16 g f g a bes c8 d4 d8 c bes | c a a a4 a8 a\glissando( g) g g\glissando( f4) | a8 g g g g g a8 g g g f g |
	\time 6/8
	a4.\< a16 bes c bes a bes
	\time 12/8
	d16\f c bes c a bes c d c bes c a bes8 c d f e d | bes4. a16 bes c d c bes g8 g a bes c c | c4\> c8 bes4 bes8 a4 a8 bes4.\! |
	
% Outro --------------------------------------------------

	f4.\pp e d c | bes1. | a1.\fermata |



\bar "|."
}}









% -------------------------------------- M A I N --------------------------------------------

\header {
  title = "Little Girl Blue"
  composer = "Janis Joplin"
  tagline = ##f
}

\score {

  <<
	\Sax
    \Geige
	\Cello
  >>

  \midi {
    \tempo 4. = 44
  }

  \layout {}

}





