% George Gershwin - Summertime

\header {
  title = "Summertime"
  composer = "George Gershwin"
  arranger = "Janis Joplin / Maike"
  tagline = ##f
}

vl = {
  \transpose g a {
    \relative c''{

      \clef "treble"
      \key bes \major
      \time 6/8
      \tempo 4. = 56
      \compressFullBarRests


    % Intro --------------------------------------------------

      R2.*17 | r4. f8\mf( d4) |

    % 1st verse ----------------------------------------------

      f4.~^"1st verse"( f8 g) d( | c4.) c8( bes4~) | bes4. r8 des8( bes | c bes4) g8( bes g~ | g) bes4 g4.~ | g2. |
      r | r4. r8 g( bes~ | bes4.) r8 d( es) | d4.~( d8~ d16) es( d es | c4.) r8 bes( g) |
      bes16( c d c bes8) r c\<( bes) | d\f( f d~ d4. | d4.) r8 g( f | g16 f d4~ d) es8( | d) r d g4 d8 |

    % 2nd verse ----------------------------------------------

      d2.~\>^"2nd verse" | d~\mf | d4. r8 c( bes) | f'4( g8~ g8) r4 | des8 c bes~ bes g4~ | g4. r8 bes\p( c16 bes) |
      des4 c8~ c8. d16( c bes | c d c bes c8~ c bes4) | r4 r8 r bes( g | bes g bes g bes g16 f | g8 bes4) r a8\mf | d d d d f d |
      c16( bes g4 ~ g4.) | r4 r8 r g'\p g | g4.~( g8 fis4~ | fis4.~ fis4 g8~ |

    % Interlude ----------------------------------------------

      g4.) r4. | R2.*16 | r4. d8\mf bes des |

    % 3rd verse ----------------------------------------------

      d4.~^"3rd verse" d8 bes( d | c4.) c8( bes4~) | bes4. r8 des8( bes | c bes4) g8( bes g~ | g) bes4 g4.~ | g2. |
      r | r4. r8 g( bes~ | bes4.) r8 d( es) | d4.~( d8~ d16) es( d es | c4.) r8 bes( g) |
      bes16( c d c bes8) r c\<( bes) | d\f( f d~ d4. | d4.) d8\>( f d) |

    % Outro --------------------------------------------------

      c16( bes g4 ~ g4.\!) | r2. | r4. d'4.~\p( | d2.~ | d4. b4.~ | b2.\fermata ) |


      \bar "|."
    }
  }
}

sax = {
  \transpose g a {
    \transpose bes c' {
      \relative c'{
        \transposition bes

        \clef "treble"
        \key bes \major
        \time 6/8
        \tempo 4. = 56
        \compressFullBarRests


      % Intro --------------------------------------------------

        g8^"Intro"\mf a bes d g a | bes c d es fis a | g fis g d4. | g8 fis g es4. |
        g8 fis g d16 es d c bes a | bes a g fis g a bes8 c es | d4. d8 c bes | c4. c8 bes a | bes( d4) c4. | bes a8 g fis |
        g4. a | bes c | d es8 d c16 bes | a8 bes a g fis4 | g2. | a2. | bes4. c4. | d4. es8 d4 |

      % 1st verse ----------------------------------------------

        g,4. r | r2. | r2. | r2. | g8\p bes d fis, bes d | d, es f g a c | bes4. d4 r8 | d4 d8 c c c | bes g g~ g r4 | r2. | r |
        r | r8 c4 c8 bes g | g16 f g f d f g4. | g4 f8 d16 f d des c bes | c8 d d16 c bes8 d4 |

      % 2nd verse ----------------------------------------------

        g4. r | r2. | r2. | r2. | g8 bes d fis, bes d | d, es f g a c | bes g4 g f8 | g2. |
        bes8 a g f es d | c bes c d es f | es4. d4 c8 | fis4. g8 a as | g fis g a16 bes a g fis g | a8 bes c d es4 | d2. | fis,4. r |

      % Interlude ----------------------------------------------

        g'8^"Interlude"\f fis g d4. | g8 fis g es4. | g8 fis g d16 es d c bes a | bes8 a g fis a d | bes4. bes8 a g | c, f a~ a r4 | bes,8 d g~ g r4 | a,8 d fis~ fis r4 | 
        d'16 es c es bes es c es bes es c es | d4. c4 bes8 | es16 es d es c es d es c8 bes | c d es d4 c8 |

        % original:
        a16 d c d bes d a d bes d a d | bes es d es c es bes es c es bes es | a, d c d bes d g, c bes c a c | f, bes a bes g bes e, a g8 fis
        % alternative:
        % a c bes a bes a | bes d c bes c bes | a c bes g bes a | f a g e g fis|

        d16 g bes d bes g d g bes d bes g | c, es g c g es d\> fis a d a fis |

      % 3rd verse ----------------------------------------------

        g4.^"3rd verse"\p r | r2. | r2. | r2. | g8 bes d fis, bes d | d, es f g a c | bes4. d4 r8 | d4 d8 c c c | bes g g~ g r4 | r2. | r |
        r | r8 c4\< c8 bes g | d'4. fis |

      % Outro --------------------------------------------------

        g8^"Outro"\mf fis g d4. | g8 fis g es4. | g8 fis g d16 es d c bes a | bes^"rit." a g fis g a bes8 c d | b2.~ | b\fermata |


        \bar "|."
      }
    }
  }
}


vlc = {
  \transpose g a {
    \relative c {

      \clef "bass"
      \key bes \major
      \time 6/8
      \tempo 4. = 56
      \compressFullBarRests


    % Intro --------------------------------------------------

      R2.*10 |
      g8\mf a bes c d es | f g a bes c es | d2. | c4. d4 c8 | d bes c bes a g | f g a g f es | d c d es f g | es\> c d bes c a |

    % 1st verse ----------------------------------------------

      g\p bes d fis, bes d | f, bes d g8 d bes | e,8 bes' des g8 des bes | es,8 bes' c g'8 c, bes | g8 a bes c d es | f g a bes c d |
      es2. | e4.~ e8 r4 | r4. c,4^"pizz." g8 | c,4. c'8 bes g | c,4. c'8 bes g |
      c, c' c c bes f | d d d d d d | es es es es es es | d d d c c c | bes4. r |

    % 2nd verse ----------------------------------------------

      g'8^"arco" bes d fis, bes d | f, bes d g8 d bes | e,8 bes' des g8 des bes | es,8 bes' c g'8 c, bes | g8 a bes c d es | f g a bes c d |
      R2.*6 | g,,8 a bes c d es | f g a bes c d | es d c d bes c | a bes a f g es |

    % Interlude ----------------------------------------------

      g,\f a bes c d es | f g a bes c es | d2. | c4. d4 c8 | a bes c d bes4 | bes8 a bes a g f | es d c d es d | c bes a g a bes |
      c d es f g a | bes4 d8 es d c | c, d es f g a | bes4 es8 d c bes |
      g a bes c d es | d c d bes c a | bes a g a f g | es f f16 d bes8 c d |
      bes16 d g bes g d bes d g bes g d | g, c e g e c a\> d fis a fis d |

    % 3rd verse ----------------------------------------------

      g,8\p bes d fis, bes d | f, bes d g8 d bes | e,8 bes' des g8 des bes | es,8 bes' c g'8 c, bes |
      g8 a bes c d es | f g a bes c d | es2. | e4.~ e8 r4 | r4. c,4^"pizz." g8 | c,4. c'8 bes g | c,4. c'8 bes g | c, c' c c bes f | d d d d d d | es r4 r4. |

    % Outro --------------------------------------------------

      g8^"arco"\mf bes c d es f | g a bes c d es | c d bes c a bes | g f16 e es d c8 d16 bes a8 | g2.~ | g\fermata |


      \bar "|."
    }
  }
}
