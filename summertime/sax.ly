\include "summertime.ly"

\version "2.18.2"
\include "../paper.ly"

\header {
  instrument = "Sopransaxophon"
}

\score {
  \new Staff \with {
    % instrumentName = #"Sopransaxophon "
    % shortInstrumentName = #"Sax. "
    midiInstrument = #"soprano sax"
  } \sax

  \midi {
    \tempo 4. = 56
  }

  \layout {}
}
