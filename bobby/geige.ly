\include "bobby.ly"

\version "2.18.2"
\include "../paper.ly"

\header {
  instrument = "Violine"
}

\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = #"Violine"
      midiInstrument = #"pizzicato strings"
    } \vl

    \new DrumStaff \with {
      instrumentName = #"Percussion"
    } \perc
  >>

  \midi {
    \tempo 2 = 72
  }

  \layout {
    \context {
      \DrumStaff \RemoveEmptyStaves
    }
  }
}
