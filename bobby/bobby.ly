% Janis Joplin - Me and Bobby McGee

\header {
  title = "Me and Bobby McGee"
  composer = "Janis Joplin"
  arranger = "Maike"
  tagline = ##f
}

vlc = <<
  \new Voice = "melodie" {
    \relative c {

      \clef "bass"
      \key g \major
      \time 4/4
      \tempo 2 = 72
      \compressFullBarRests


      % Intro --------------------------------------------------

      R1*8 |

      % 1st verse ----------------------------------------------

      d8^"1st verse"\f d4 e8~ e4 d | bes8 b4. r2 | r4 d8 e8~ e4 d8 c | b2 r4. g8 | d'4 d8 d~ d4 d | c8 b a4 g4. a8~ | a1 | r |
      c8 c4 d8~ d4. c8 | a4 a8 b~ b2 | r4 c8 d~ d c b a~ | a2 r4 r8 c | c c4 d8~ d4. c8 | b( a4) g8 a g4 b8~ | b1 | r2 r4. c8 |

      % 2nd verse ----------------------------------------------

      d2^"2nd verse" e8 d c b~ | b2 r4 c | d8 d4 e8~ e d4 b8~ | b2 r4 d | d8 d4. d4 e | f8 f4. e4 d | e2. d8( c~ | c2)  r |
      g'4 g g g | a8 g4 f8~ f e4 d8~ | d d4 d8~ d d4 e8~ | e d c4 r4. b8 | a2 b8 a4. | b4 a b8 a4 d8~ | d1 | r |

      % chorus -------------------------------------------------

      e4^"chorus" e e fis8 g~ | g fis e2 e4 | d8 c b2 c8 d~ | d2 r | b8 a4. r4 g'8 g | b a4 g8 e4 g | g8( e d2) a4( | b2) r4 g' |
      e e e g | a g8 a~ a4 g~ | g2 f8 g4 g8 | f16( e d8~ d4) r4 c8 b | a4 a a b | c b8 c~ c4 b8 d~ | d2 r4 e8 fis~ | fis2 r |
      a,4 a8 a~ a4 b8 c~ | c4 d b8 b4 a8 | g1 | r | r | r2 r4 cis8 d |

      % 3rd verse ----------------------------------------------

      \key a \major
      e2^"3rd verse" e8 e4. | fis8( e d4) r cis8 d | e4 fis8 e~ e4 d8 cis~ | cis2 r4 e8 e~ | e e4 e8~ e8 d4 cis8~ | cis b4( a8~ a4.) b8~ | b1 | r2 r4 d8 d~ |
      d2 e4 cis8 e~ | e fis4. r4 r8 cis | e cis4 e8~ e4 cis8 fis~ | fis2 r4 r8 a | cis8 b4 a8~ a fis4 r8 | a4 fis cis b | cis b8( a~ a2) | r2 r4 e' |

      % 4th verse ----------------------------------------------

      e^"4th verse" dis e d | e8( d) a'( b a2) | r8 fis a4 fis a | a8( fis e4) r fis | e8 e4. e8 fis4 g8~ | g4 r8 g a4 a | fis8( e d2) r4 | r2 r8 a' a4 |
      c a c a8 c~ | c a~a r8 r a a4 | c4 a8( g~ g4) a4 | a8( g e4) r8 e e4 | b b b cis8 d~ | d d r4 b a8 e'~ | e1 | r |

      % chorus -------------------------------------------------

      fis4^"chorus" fis fis gis8 a~ | a gis fis2 fis4( | e) e8 d cis4 d8 e~ | e2 r | cis8 b4. r4 a'8 a | cis b4 a8 b4 a | a8( fis e2) r8 b( | cis2) r4 a'8 a |
      fis4 fis fis a | b a8 b~( b4 a~ | a2) g8 a4 a8 | g16( fis e8~ e4) r d8 cis | b4 b b cis | d cis8 d( cis4) b8 e~ | e2 r4 fis8 gis~ | gis2 r |
      b,4 b8 b~ b4 cis8 d~ | d4 e cis8 cis4 b8 | a1 | r |

      \bar "|."
    }
  }
  \new Lyrics \lyricsto "melodie" {
    \lyricmode {
      Bus -- ted flat in Baton Rouge, wai -- tin' for a train
      And_I's fee -- lin' near as fa -- ded as my jeans
      Bob -- by thumbed a die -- sel down, just be --fore it rained
      It rode us all the way to New Or -- leans

      I pulled my har -- _ poon out_of_my dir -- ty red ban -- danna
      I_was play -- in' soft while Bob -- by sang the blues, yeah
      Wind -- shield wi -- pers slap -- pin' time, I was holdin' Bob -- by's hand in mine
      We sang ev -- ery song that dri -- ver knew

      Free -- dom's just an -- o -- ther word for no -- thin' left to lose
      No -- thin', don't mean no -- thin' hon' if_it ain't free, no_no
      And, fee -- lin' good was ea -- sy, Lord, when he sang the blues
      You know, fee -- lin' good was good e -- nough for me _ _
      Good e -- nough for me and_my Bob -- by Mc -- Gee

      From the Ken -- tuc -- ky coal_mine to the Ca -- li -- for -- nia sun
      There Bob -- by shared the se -- crets_of my_soul
      Through all kinds of wea -- ther, through eve -- ry -- thing we done
      Yeah, Bob -- by ba -- by kept me from the cold _
      
      One day up near Sa -- linas, Lord, I let him slip away
      He's loo -- kin' for that home, and_I hope he finds_it
      But, I'd trade all_of my to -- mor -- rows, for a sin -- gle yester -- day
      To be hol -- din' Bob -- by's bo -- dy next to mine

      Free -- dom's just an -- o -- ther word for no -- thin' left to lose
      No -- thin', that's _ all that Bob -- by left me, yeah
      But, _ fee -- lin' good was ea -- sy, Lord,_when he sang the blues
      Hey, _ fee -- lin' good was good e -- nough for me, mm -- hmm
      Good e -- nough for me and_my Bob -- by Mc -- Gee
    }
  }
>>

sax = {
  \transpose bes c' {
    \relative c'' {
      \transposition bes

      \clef "treble"
      \key g \major
      \time 4/4
      \tempo 2 = 72
      \compressFullBarRests

      \set Staff.midiMaximumVolume = #0.8


      % Intro --------------------------------------------------

      R1*8 |

      % 1st verse ----------------------------------------------

      R1*15 | r2 r4 r8 c,\p( |

      % 2nd verse ----------------------------------------------

      b1) | r4 r8 c( d b') a4 | b1 | r4 r8 b b( a g4) | b2. c4 | d1 | <c e>^"tr." | r |
      r | r4 e d8 c4 b8~ | b1 | r8 c4 g'8 c,( b) g4 | a1 | r | r4 fis a,8( g') a,4 | a'2 d4 d,8( d') |

      % chorus -------------------------------------------------

      e1 | r2 g4 g,8( g') | <b, d>1^"tr." | r8 c,( d) b' r des, bes'4 | a1 | r1 | r | r2 d8( a) g4 |
      e1 | r | r |  r8 c( d) b' r des, bes'4 | a1 | r | r2 fis4 d8 a'~ | a d d4 d2~ |
      d1 | r | r8 bes a g e d b g'~ | g4 e d r | r1 | r |

      % 3rd verse ----------------------------------------------

      \key a \major
      r1 | a8 cis a'2~ a8 b | a2 r | r4 cis2~ cis8 d | cis2 r | r2 cis4 c | b2~ b8 g gis b | e,2 r4 g8( gis) |
      e8( e'4.) r4 g,8( gis) | b4 b b8 bes a8 gis~ | gis2 r | r r4 r8 e | a,8( a'4.) r2 | r4 r8 cis b a fis e | a4 a fis8 e c( b) | a4. c8( cis) e fis4 |

      % 4th verse ----------------------------------------------

      a2 r | r r4 b | cis2 r | r a8 e d4 | cis8 a'~ a4 d,8 b'4 cis8~ | cis4 d e2 | fis1 | r2 fis8 a, d4 |
      fis1~ | fis8 a, fis'4 e d | e4.( d8) a'2~ | a1 | r | r | r4 gis, b,8( a') b,4 | b'2 e4 e,8( e') |

      % chorus -------------------------------------------------

      fis1 | r2 a4 a,8( a') | <cis, e>1^"tr." | r8 d,( e) cis' r es, c'4 | b1 | r1 | r | r2 e8( b) a4 |
      fis1 | r | r | r8 d( e) cis' r es, c'4 | b1 | r | r2 gis4 e8 b'~ | b e e4 e2~ |
      e1 | r | r | r |



      \bar "|."
    }
  }
}

vl = {
  \relative c'' {

    \clef "treble"
    \key g \major
    \time 4/4
    \tempo 2 = 72
    \compressFullBarRests

    \set Staff.midiMaximumVolume = #0.7


    % Intro --------------------------------------------------

    <b, d>4^"Intro"\f <g' b> <g b>8 <g b> <g b> <g b> | <g b> <g c> <g c> <g c> <g c> <g c> <g c> <g b> |
    <b, d>4\> <g' b>~ <g b>8 <g c> <g c> <g b> | <b, d>4 g^"pizz." <g' b>8-"arco" <g c> <g c> <g b> |
    g,8 r g\!^"pizz." r b c c b | g r g r r c c b | g r g r r c c b | g r g r r c c b |

    % 1st verse ----------------------------------------------

    g r g r b8 d d b | g r g r r d' d b | g r g r r d' d b | g r g r r d' d b | g r g r r d' d b | g r g r r d' d b | d r b'4^"arco" <a c>8 <g c> fis d'~ | d r d,^"pizz." r r a' a fis |
    d r d r r a' a fis | d r d r r a' a fis | d r d r r a' a fis | d r d r r a' a fis | d r d r r a' a fis | d r d r r a' a fis | g, r g r r b'^"arco" g' e | d bes a g e d b a |

    % 2nd verse ----------------------------------------------

    g r g^"pizz." r r d' d b | g r g r r d' d b | g r g r r d' d b | g r g r r d' d b | g r g r r d' d b | g r g r r f' d b | c r g r e' g g e | c r g r r g' g e |
    c r g r r g' g e | c r g r r g' d b | g r g r b d d b | g r g r r d' d b | d r d r fis8 a a fis | d r d r r a' a fis | d r d r r a' a fis | d r d r r a' g e |

    % chorus -------------------------------------------------

    c r g r e' g g e | c r g r r g' d b | g r g r b d d b | g r g r r d' d b | d r d r fis a a fis | d r d r r g d b | g r r4 r4 b~^"arco" | b8 a b a b a g4 |
    c g8^"pizz." r e' g g e | c r g r r g' g e | g, r g r b d d b | g r g r r d' d b | d r d r fis a a fis | d r d r r a' a fis | d r d r r a' a fis | d r d r r a' a fis |
    d r d r r a' a fis | d r d r r a' a fis | d8 r d r r a' a fis | d r d r r2 | a'8^"arco" a a a a a e' c | b a fis e a f e a,~ | 

    % 3rd verse ----------------------------------------------

    \key a \major
    a r r4 r8 e'^"pizz." e cis | a r r4 r8 e' e cis | a r r4 r8 e' e cis | a r r4 r8 e' e cis | a r r4 r8 e' e cis | a r r4 r8 e' e cis | e r r4 gis8 b b gis | e r r4 r8 b' b gis |
    e r r4 r8 b' b gis | e r r4 r8 b' b gis | e r r4 r8 b' b gis | e r r4 r8 b' b gis | e r r4 r8 b' b gis | e r r4 r8 a e cis | a r r4 cis8 e e cis | a r r4 r8 e' e cis |

    % 4th verse ----------------------------------------------

    a r r4 r8 e' e cis | a r r4 r8 e' e cis | a r r4 r8 e' e cis | a r r4 r8 e' e cis | a r r4 r8 e' e cis | a r r4 r8 g' e cis | d r r4 fis8 a a fis | d r r4 r8 a' a fis |
    d r r4 r8 a' a fis | d r r4 r8 a' a fis | a r r4 e8 a e cis | a r r4 r8 e' e cis | a r r4 r8 e' e cis | a r r4 r8 e' e cis | e r r4 gis8 b b gis | e r r4 r8 b' a fis |

    % chorus -------------------------------------------------

    d r d r fis8 a a fis | d r d r r a' a fis | a r a r e8 a e cis | a r a r r e' e cis | e r e r gis8 b b gis | e r e r r a e cis | a r a r cis8 e e cis | a r a r r g' e cis |
    d r d r fis8 a a fis | d r d r r a' a fis | a r a r e8 a e cis | a r a r r e' e cis | e r e r gis8 b b gis | e r e r r b' b gis | e r e r r b' b gis | e r e r r b' b gis |
    e r e r r b' b gis | e r e r r a e cis | a r a r cis8 e e cis | a r r4 r r |

    \bar "|."
  }
}

perc = {
  \drummode {

    \clef percussion
    \time 4/4
    \tempo 2 = 72
    \compressFullBarRests

  % Intro --------------------------------------------------

    r1 | r | r | r |
    r4 ssl^"Violine" r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 |

  % 1st verse ----------------------------------------------

    r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r1 | r4 ssl r2 |
    r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r1 |

  % 2nd verse ----------------------------------------------

    r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 |
    r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 |

  % chorus -------------------------------------------------

    r1 | r | r | r | r | r | r | r |
    r | r | r | r | r | r | r | r |
    r | r | r | r | r | r |

  % 3rd verse ----------------------------------------------

    r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 |
    r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 |

  % 4th verse ----------------------------------------------

    r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 |
    r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 | r4 ssl r2 |

  % chorus -------------------------------------------------

    r1 | r | r | r | r | r | r | r |
    r | r | r | r | r | r | r | r |
    r | r | r | r |

    \bar "|."
  }
}
