\include "bobby.ly"

\version "2.18.2"
\include "../paper.ly"

\header {
  instrument = "Sopransaxophon"
}

\score {
  \new Staff \with {
    midiInstrument = #"soprano sax"
  } \sax

  \midi {
    \tempo 2 = 72
  }

  \layout {}
}
