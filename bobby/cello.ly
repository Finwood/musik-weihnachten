\include "bobby.ly"

\version "2.18.2"
\include "../paper.ly"

\header {
  instrument = "Cello"
}

\score {
  \new Staff \with {
    midiInstrument = #"cello"
  } \vlc

  \midi {
    \tempo 2 = 72
  }

  \layout {}
}
