\include "bobby.ly"

\version "2.18.2"
\include "../paper.ly"
#(set-global-staff-size 14)

\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = #"Cello"
      shortInstrumentName = #"Vc."
      midiInstrument = #"cello"
    } \vlc

    \new Staff \with {
      instrumentName = #"Sopransaxophon"
      shortInstrumentName = #"Sax."
      midiInstrument = #"soprano sax"
    } \sax

    \new Staff \with {
      instrumentName = #"Violine"
      shortInstrumentName = #"Vl."
      midiInstrument = #"pizzicato strings"
    } \vl

    \new DrumStaff \with {
      instrumentName = #"Percussion"
      shortInstrumentName = #"Perc."
    } \perc
  >>

  \midi {
    \tempo 2 = 72
  }

  \layout {
    \context {
      \DrumStaff \RemoveEmptyStaves
    }
  }
}
